const colors = ['blue', 'yellow', 'black', 'red'];
const stones = [];

// Joker selecting
const jokerStone = Math.floor(Math.random() * (13 - 1 + 1)) + 1;
const jokerColor = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
console.log('Okey ', { stone: jokerStone, color: colors[jokerColor] });

// Stones creator and shuffle
stones.push({ stone: jokerStone, color: colors[jokerColor], status: '2' });
stones.push({ stone: jokerStone, color: colors[jokerColor], status: '2' });
for (let i = 0; i < colors.length; i++) {
  for (let j = 1; j <= 13; j++) {
    stones.push({ stone: j, color: colors[i], status: '2' });
    stones.push({ stone: j, color: colors[i], status: '2' });
  }
}

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

const shuffledStones = shuffle(stones);

// Stone deal to users
const extra = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
const users = [];
for (let i = 0; i < 4; i++) {
  const max = i == extra ? 15 : 14;
  const stones = [];
  for (let j = 1; j <= max; j++) {
    stones.push(shuffledStones.pop());
  }
  stones.sort((a, b) => {
    return a.stone - b.stone;
  });
  const series = seriesCheck(stones);
  const double = doubleCheck(stones);
  users.push({
    id: i + 1,
    stones,
    stoneCount: stones.length,
    series,
    double,
    total: series + double,
  });
}

const optimalWinner = users.sort((a, b) => {
  return a.total - b.total;
});
console.log('Kazanan oyuncu : ' + optimalWinner[0].id);
console.log('Kazanan oyuncu detay : ', optimalWinner[0]);

// Double stone check and return count
function doubleCheck(array) {
  const tmpArray = array.filter(
    (x) => x.stone != jokerStone && x.color != jokerColor,
  );
  let doubleCount = 0;
  for (let i = 0; i < tmpArray.length; i++) {
    const count = tmpArray.filter(
      (x) => x.color == tmpArray[i].color && x.stone == tmpArray[i].stone,
    );
    if (count.length > 1) doubleCount = doubleCount + 1;
  }
  let count = 7 - doubleCount / 2;
  if (
    array.some((x) => x.stone == jokerStone && x.color == colors[jokerColor])
  ) {
    count = count - 1;
  }
  return count;
}

// Series stone check and return optimal count
function seriesCheck(array) {
  const tmpArray = array.filter(
    (x) => x.stone != jokerStone && x.color != jokerColor,
  );
  for (let i = 0; i < tmpArray.length; i++) {
    if (tmpArray[i].status == 'taken') continue;
    let tmpStones = [];
    let tmpColors = [];

    const stoneGroup = tmpArray.filter((x) => x.stone == tmpArray[i].stone);
    for (let j = 0; j < colors.length; j++) {
      const tmpStone = stoneGroup.find((x) => x.color == colors[j]);
      if (tmpStone) {
        tmpColors.push(tmpStone);
      }
    }

    const colorGroup = tmpArray.filter((x) => x.color == tmpArray[i].color);
    for (let j = tmpArray[i].stone; j < 15; j++) {
      if (j == 14) j = 1;
      const tmp = colorGroup.find((x) => x.stone == j);
      if (tmp) {
        tmpStones.push(tmp);
      } else {
        break;
      }
    }
    if (tmpColors.length > 2 || tmpStones.length > 2) {
      if (tmpColors.length > tmpStones.length) {
        for (let j = 0; j < tmpColors.length; j++) {
          const tmp = array.find(
            (x) =>
              x.stone == tmpColors[j].stone && x.color == tmpColors[j].color,
          );
          tmp.status = 'taken';
        }
      } else {
        for (let j = 0; j < tmpStones.length; j++) {
          const tmp = array.find(
            (x) =>
              x.stone == tmpStones[j].stone && x.color == tmpStones[j].color,
          );
          tmp.status = 'taken';
        }
      }
    } else if (tmpColors.length == 2 || tmpStones.length == 2) {
      if (tmpColors.length > tmpStones.length) {
        for (let j = 0; j < tmpColors.length; j++) {
          const tmp = array.find(
            (x) =>
              x.stone == tmpColors[j].stone && x.color == tmpColors[j].color,
          );
          tmp.status = '1';
        }
      } else {
        for (let j = 0; j < tmpStones.length; j++) {
          const tmp = array.find(
            (x) =>
              x.stone == tmpStones[j].stone && x.color == tmpStones[j].color,
          );
          tmp.status = '1';
        }
      }
    }
  }

  let count =
    tmpArray.filter((x) => x.status == '1').length / 2 +
    tmpArray.filter((x) => x.status == '2').length * 2;

  // If joker exists find optimal location
  if (
    array.some((x) => x.stone == jokerStone && x.color == colors[jokerColor])
  ) {
    const status2 = tmpArray.filter((x) => x.status == '2');
    let isJokerUsed = false;
    for (let i = 0; i < status2.length; i++) {
      let tmpCount = 0;
      for (let j = status2[i].stone + 2; j < 15; j++) {
        if (j == 14) j = 1;
        const tmp2 = tmpArray.some(
          (x) => x.stone == j && x.color == status2[i].color,
        );
        if (tmp2) tmpCount = tmpCount + 1;
        else {
          if (tmpCount == 2) {
            count = count - 3;
            isJokerUsed = true;
          }
          break;
        }
      }
    }
    if (!isJokerUsed && tmpArray.some((x) => x.status == '1')) {
      count = count - 2;
    }
  }
  return count;
}