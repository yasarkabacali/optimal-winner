# Optimal Winner Finder

It's a `nodejs(>=node v10)` console script. 

You can run it with the following code.

```bash
node index.js
```

Winning player information is provided on the console.